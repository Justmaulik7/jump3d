﻿
using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameScreen : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_Visual;
    [SerializeField] private TextMeshProUGUI m_CheerText;
    [SerializeField] private TextMeshProUGUI m_LevelText;
    [SerializeField] private Slider m_LevelProgress;
    [SerializeField] private TextMeshProUGUI m_LevelDown;
    [SerializeField] private TextMeshProUGUI m_Levelup;

    private Tween m_CheerTween;

    [Button]
    private void Refs()
    {
        m_Visual = GetComponentInChildren<CanvasGroup>();
        m_CheerText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void Start()
    {
        m_LevelProgress.value = 0;
        m_Visual.gameObject.SetActive(false);
        m_Visual.DOFade(0, 0);
        m_CheerText.text  ="";
        GameManager.PerfectJump.AddListener(OnPerfect);
        GameManager.LevelStart.AddListener(OnLevelStart);
        GameManager.LevelWin.AddListener(OnLevelWin);
        m_LevelText.text = "Level " + GameManager.Level;
        m_LevelDown.text = GameManager.Level.ToString();
        m_Levelup.text = (GameManager.Level + 1 ).ToString();

    }

    private void OnLevelWin()
    {
        m_Visual.DOFade(0, 0.25f);
    }

    private void OnLevelStart()
    {
        m_Visual.gameObject.SetActive(true);
        m_Visual.DOFade(1, 0.25f);
        
    }

    private void OnPerfect()
    {
        m_CheerTween?.Kill(true);
        m_CheerText.text = Random.value < 0.5f ? "Cool!" : "Perfect!";
        m_CheerText.transform.localEulerAngles = Vector3.forward * Random.Range(-10, 10);
        m_CheerText.transform.localScale = Vector3.zero;
        m_CheerTween = m_CheerText.transform.DOScale(Vector3.one, 0.7f).OnComplete(() =>
        {
            m_CheerTween = m_CheerText.transform.DOScale(Vector3.zero, 0.25f);
        });

    }

    private void Update()
    {
        if (!GameManager.IsPlaying && GameManager.ActiveCharacter != null && GameManager.ActiveLevel == null) return;
        m_LevelProgress.maxValue = 1;
        m_LevelProgress.value = (float)GameManager.ActiveLevel.Path.Project(GameManager.ActiveCharacter.transform.position,0,1).percent;
    }
}
