﻿using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Platform : MonoBehaviour
{
    [SerializeField] private Transform m_Visual;
    [SerializeField] private Transform m_BrokenVisual;
    [SerializeField] private List<BrokenData> m_PartsData;
    [SerializeField] private ParticleSystem m_ParticleSystem;
    [SerializeField] private bool m_Move;
    [SerializeField,ShowIf("m_Move")] private Vector2 m_Range;
    [SerializeField] private List<SpriteRenderer> m_Colors = new List<SpriteRenderer>();

    [SerializeField] private TextMeshPro m_PlatformText_1;
    [SerializeField] private TextMeshPro m_PlatformText_2;
    [SerializeField] private Rigidbody m_Board;
    
    private Tween m_MoveTween;

#if UNITY_EDITOR
    [Button]
    private void Ref()
    {
        m_Visual = transform.GetChild(0);
        m_BrokenVisual = transform.GetChild(1);
        transform.eulerAngles = new Vector3(0,transform.eulerAngles.y,0);
        
        m_PartsData.Clear();
        foreach (Transform item in m_BrokenVisual)
        {
            m_PartsData.Add(new BrokenData(){Position = item.localPosition,Angle = item.localEulerAngles,RigidBody = item.GetComponent<Rigidbody>()} );
        }
    }
#endif
   

    private void Start()
    {
        m_Visual.gameObject.SetActive(true);
        m_BrokenVisual.gameObject.SetActive(false);
        m_Move = Random.value < 0.2f && transform.GetSiblingIndex() != 0 && transform.GetSiblingIndex() != transform.parent.childCount-1;
        if (m_Move) DOVirtual.DelayedCall(Random.value, LoopMove);
        ColorsData lcolor = GameManager.Insatnce.GameData.TrempolColorsData[ Random.Range(0, GameManager.Insatnce.GameData.TrempolColorsData.Count)];
        for (int i = 0; i < m_Colors.Count; i++)
        {
            if (i%2 == 0)
            {
                m_Colors[i].color = lcolor.ColorA ;
            }
            else
            {
                m_Colors[i].color =  lcolor.ColorB;
            }
        }

        m_PlatformText_1.text = m_PlatformText_2.text = (transform.parent.childCount-transform.GetSiblingIndex()).ToString();
    }

    [Button]
    public void Explode()
    {
        m_Visual.gameObject.SetActive(false);
        m_BrokenVisual.gameObject.SetActive(true);
        m_Board.transform.parent = null;
        m_BrokenVisual.parent = null;
        m_Board.isKinematic = false;
        m_Board.AddTorque(Random.rotation.eulerAngles,ForceMode.Impulse);
        m_Board.AddForce(Random.rotation.eulerAngles.normalized*5,ForceMode.Impulse);
        DOVirtual.DelayedCall(5,()=>{Destroy(m_Board.gameObject);});
        for (int i = 0; i < m_PartsData.Count; i++)
        {
            var l_Part = m_BrokenVisual.GetChild(i);
            l_Part.localPosition = m_PartsData[i].Position;
            l_Part.localEulerAngles = m_PartsData[i].Angle;
            m_PartsData[i].RigidBody.AddTorque(Random.rotation.eulerAngles,ForceMode.Impulse);
            m_PartsData[i].RigidBody.AddForce(Random.rotation.eulerAngles.normalized*5+Vector3.down*2,ForceMode.Impulse);
           // m_PartsData[i].RigidBody.AddExplosionForce(200,transform.position-Vector3.up*-5,50);
        }
      
       

        DOVirtual.DelayedCall(6, () =>
        {
            m_BrokenVisual.gameObject.SetActive(false);
        });
    }

    public void PlayParticle()
    {
        m_ParticleSystem.Play();
    }

    private void LoopMove()
    {
        m_MoveTween?.Kill(true);
       
        m_MoveTween = m_Visual.DOLocalMoveX(m_Range.x, 2).SetEase(Ease.Linear).OnComplete(() =>
        {
            m_MoveTween = m_Visual.DOLocalMoveX(m_Range.y, 2).SetEase(Ease.Linear).OnComplete(LoopMove);
        });
    }

    private void LateUpdate()
    {
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }
  
}

[System.Serializable]
public class BrokenData
{
    public Vector3 Position;
    public Vector3 Angle;
    public Rigidbody RigidBody;
}
