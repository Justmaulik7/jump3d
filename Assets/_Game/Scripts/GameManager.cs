﻿using System;
using System.Threading;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static bool IsPlaying = false;
    public static GameManager Insatnce;
    public static UnityEvent PerfectJump = new UnityEvent();
    public static UnityEvent GameOver = new UnityEvent();
    public static UnityEvent LevelStart = new UnityEvent();
    public static UnityEvent LevelWin = new UnityEvent();

    public static Level ActiveLevel;
    public static Character ActiveCharacter;
    public GameConfig GameData;

    public ParticleSystem Waterripple;
    public ParticleSystem Win_particlseffcts;
    public Character Players;
    public Transform m_CameraRotet;
   public  Transform m_Island;
    public static int Level
    {
        get => PlayerPrefs.GetInt(nameof(Level), 1);
        set => PlayerPrefs.SetInt(nameof(Level),value);
    }

    private void Awake()
    {
        if (Insatnce == null) Insatnce = this;
    }

    private void Start()
    {
        PerfectJump.AddListener(OnPerfectJump);
        LevelStart.AddListener(OnLevelStart);
        GameOver.AddListener(OnGameOver);
        LevelWin.AddListener(OnLevelWin);
    }

    private void OnLevelWin()
    {
        IsPlaying = false;
        Level++;
    }

  
    public static void DoSlowMo()
    {
        Time.timeScale = 0.1f;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    }

    public static void EndSlowMo()
    {
       Time.timeScale = 1;
       Time.fixedDeltaTime = 0.02F ;
    }


    private void OnGameOver()
    {
        IsPlaying = false;
        var lwaterRippel = Instantiate(Waterripple);
        lwaterRippel.transform.position = new Vector3( Players.transform.localPosition.x,Players.transform.localPosition.y, Players.transform.localPosition.z);
        Destroy(lwaterRippel, 1f);
        DOVirtual.DelayedCall(1f, () =>
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneBuildIndex: 0);
        });
    }

    private void OnLevelStart()
    {
        IsPlaying = true;
        
    }

    private void OnPerfectJump()
    {
        Debug.Log("OnPerfectJump");
    }


    private void Update()
    {
        if (!IsPlaying && Input.GetKeyDown(KeyCode.Space))
        {
            LevelStart?.Invoke();
        }
    }
}
