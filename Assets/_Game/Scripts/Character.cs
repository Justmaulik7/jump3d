﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class Character : MonoBehaviour
{

    public static Character Instance;
    [SerializeField] private bool m_IsPlayer;
    [SerializeField] private Rigidbody m_Rigidbody;
    [SerializeField] private LineRenderer m_LineRenderer;
    [SerializeField] private float mJumpForce;
    [SerializeField] private Transform mCamRoot;
    [SerializeField] private float mPerfectJumpForce;
    [SerializeField] private Transform mTarget;

    [SerializeField] private GameObject m_PerfectFx;
    [SerializeField] private Animator m_Playersanimatir;
    [SerializeField] private Ease m_JumpEase;

   [SerializeField,ReadOnly] private Vector2 m_LastMousePosition = Vector2.zero;
   [SerializeField,ReadOnly] private Vector2 m_MouseDelta = Vector2.zero;
   [SerializeField] private float m_MouseMinDrag = 10;

   [SerializeField] private float MoveSpeed;
   private RaycastHit m_DummyHit;
   private static readonly int MDance = Animator.StringToHash("Dance");
   private static readonly int Flip = Animator.StringToHash("Flip");

   
   private bool m_Jumping;
   private bool mCanRotate = true;
   
#if UNITY_EDITOR
    [Button]
    private void Refs()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_LineRenderer = GetComponentInChildren<LineRenderer>();
    }
#endif
    
    private void Awake()
    {
        if(m_IsPlayer) GameManager.ActiveCharacter = this;
        if(Instance == null) Instance = this;
    }

    private void Start()
    {
       
        GameManager.LevelStart.AddListener(OnStart);
        m_LineRenderer.gameObject.SetActive(m_IsPlayer);
        mTarget.gameObject.SetActive(m_IsPlayer);
        m_Rigidbody.isKinematic = true;
        DOVirtual.DelayedCall(0.1f, () =>
        {
            transform.position = GameManager.ActiveLevel.Platforms.transform.GetChild(0).position+ Vector3.up*5;
            transform.rotation = GameManager.ActiveLevel.Platforms.transform.GetChild(0).rotation;
            if (!m_IsPlayer)
            {
                transform.position = GameManager.ActiveLevel.Platforms.transform.GetChild(Random.Range(1,GameManager.ActiveLevel.Platforms.transform.childCount-3)).position+ (Vector3.up*5)+ UnityEngine.Random.insideUnitSphere;
            }
        });
       
    }

    private Gradient UpdateLine(Color iColor)
    {
        var gradient = new Gradient();
        var colorKey = new GradientColorKey[2];;
        var alphaKey = new GradientAlphaKey[2];
        
        colorKey[0].color = iColor;
        colorKey[0].time = 0.0f;
        colorKey[1].color = iColor;
        colorKey[1].time = 1.0f;
        
        alphaKey[0].alpha = 0.5f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 0.9f;
        alphaKey[1].time = 1f;
        
        gradient.SetKeys(colorKey, alphaKey);
        
        return gradient;
    }

    private void OnStart()
    {
      m_Rigidbody.isKinematic = false;
    }


    private void Update()
    {
        if (m_IsPlayer && GameManager.IsPlaying && mCanRotate)
        {

            if (Input.GetMouseButtonDown(0) && m_LastMousePosition == Vector2.zero)
            {
                m_LastMousePosition = Input.mousePosition;
                m_MouseDelta = Vector2.zero;
            }
            else if (Input.GetMouseButtonUp(0) && m_LastMousePosition != Vector2.zero)
            {
                m_LastMousePosition = m_MouseDelta = m_LastMousePosition = Vector2.zero;

            }
            else if (Input.GetMouseButton(0) && m_LastMousePosition != Vector2.zero)
            {
                if (Vector2.Distance(m_LastMousePosition, Input.mousePosition) < m_MouseMinDrag)
                {
                    m_MouseDelta = Vector2.zero;
                }
                else
                {
                    m_MouseDelta = Vector2.ClampMagnitude(m_LastMousePosition - (Vector2) Input.mousePosition, 5);
                }

                //move 
                transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward,
                    Time.deltaTime * MoveSpeed);
            }

            if (Mathf.Abs(m_MouseDelta.x) > 0)
            {
                transform.eulerAngles += Vector3.up * m_MouseDelta.x * -1;
            }

            m_LastMousePosition = Input.mousePosition;

            if (Physics.Raycast(transform.position + Vector3.up * 2, transform.TransformDirection(Vector3.down),
                out m_DummyHit, Mathf.Infinity, 1 << 9))
            {
                m_LineRenderer.colorGradient = UpdateLine(Color.green);
                mTarget.position = m_DummyHit.point;
           
            }
            else
            {
                mTarget.position = transform.position + (Vector3.down * 100);
                m_LineRenderer.colorGradient = UpdateLine(Color.red);
            }

            m_LineRenderer.SetPosition(1, mTarget.localPosition);
          
            bool leffctsiscrate = true;
            if (transform.position.y < -46 && GameManager.IsPlaying)
            {
                m_Rigidbody.isKinematic = true;
                mCamRoot.parent = null;
                transform.GetChild(0).gameObject.SetActive(false);
                GameManager.GameOver?.Invoke();
            }
        }
        else
        {
            if (transform.position.y < -46)
            {
                gameObject.SetActive(false);
            }
        }
        
    }
    
    [Button]
    public void JumpToNextPlatform()
    {
        if(m_IsPlayer && m_Jumping) return;
        m_Jumping = true;
        m_Rigidbody.isKinematic = true;
        var lTarget = GetNearPlatform();
        var lIndex = lTarget.GetSiblingIndex();
        if (lIndex != lTarget.parent.childCount-1) lIndex++;
        
   
        if(Random.value < 0.5f) lTarget = lTarget.parent.GetChild(lIndex);
        var lookPos = lTarget.position - transform.position;
        lookPos.y = 0;
        transform.rotation = Quaternion.LookRotation(lookPos);
  
        DOVirtual.DelayedCall(0.5f, () => m_Playersanimatir.SetBool(Flip,true));
        DOVirtual.DelayedCall(0.6f, () => m_Playersanimatir.SetBool(Flip,false));
        var lTargetPos = lTarget.position;
        //lTargetPos.y += 0.5f;
        transform.DOJump(lTargetPos, 5, 1, 3).SetEase(m_JumpEase).OnComplete(()=>
        {
            m_Jumping = false;
            m_Rigidbody.isKinematic = false;
            m_Rigidbody.velocity += Vector3.down *2;

        });

    }

    Transform GetNearPlatform()
    {
        var lPlatform = GameManager.ActiveLevel.Platforms.transform.GetChild(0);
        var lDistance = Mathf.Infinity;

        foreach (Transform item in  GameManager.ActiveLevel.Platforms.transform)
        {
            if(Vector3.Distance(transform.position,item.transform.position)<lDistance)
            {
                lDistance = Vector3.Distance(transform.position, item.transform.position);
                lPlatform = item;
            }
        }
        return lPlatform;
    }

    private void FixedUpdate()
    {
        m_Rigidbody.velocity = Vector3.ClampMagnitude(m_Rigidbody.velocity,30);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_IsPlayer)
        {
            if (other.gameObject.layer == 9)
            {
                var l_JumpForce = mJumpForce;
                DOVirtual.DelayedCall(0.5f, () => m_Playersanimatir.SetBool(Flip,true));
                DOVirtual.DelayedCall(0.6f, () => m_Playersanimatir.SetBool(Flip,false));
                if (other.gameObject.CompareTag("Perfect") && other.transform.parent.parent.parent.GetSiblingIndex() != 0)
                {
                    CameraShake.shakeDuration += 0.1f;
                    other.transform.parent.parent.parent.GetComponent<Platform>().PlayParticle();
                    other.transform.parent.parent.parent.GetComponent<Platform>().Explode();
                    GameManager.DoSlowMo();
                    DOVirtual.DelayedCall(1, GameManager.EndSlowMo);
                    GameManager.PerfectJump?.Invoke();
                    var l_Perfect = Instantiate(m_PerfectFx, transform.position, Quaternion.identity);
                    l_Perfect.transform.eulerAngles = new Vector3(-90, 0, 0);
                    DOVirtual.DelayedCall(3, () => { Destroy(l_Perfect); });
                    l_JumpForce = mPerfectJumpForce;
                }
                m_Rigidbody.velocity = Vector3.up * l_JumpForce;
                if (other.transform.childCount == 0 && other.transform.parent.parent.parent.GetSiblingIndex() == 0) return;
                if (other.transform.parent.gameObject.CompareTag("Platform") && other.transform.parent.gameObject.GetComponent<Platform>() != null)
                {
                    other.transform.parent.gameObject.GetComponent<Platform>().PlayParticle();
                } 
            }
            
            if (other.gameObject.layer == 10 && GameManager.IsPlaying)
            {
                GameManager.LevelWin.Invoke();
                transform.position= GameManager.Insatnce.m_Island.transform.position;
                for (int i = 1; i >= -1; i--)
                {
                    if (i != 0)
                    {
                        var lwaterRippel1 = Instantiate(GameManager.Insatnce.Win_particlseffcts);
                        lwaterRippel1.transform.position = new Vector3(transform.localPosition.x - 1 * i ,transform.localPosition.y  + 1f ,transform.localPosition.z);
                        Destroy(lwaterRippel1,5f);
                        var lwaterRippel2 = Instantiate(GameManager.Insatnce.Win_particlseffcts);
                        lwaterRippel2.transform.position = new Vector3(transform.localPosition.x ,transform.localPosition.y  + 1f ,transform.localPosition.z - 1*i);
                        Destroy(lwaterRippel2,5f);
                    }
                }
                Camera.main.gameObject.SetActive(false);
                GameManager.Insatnce.m_CameraRotet.gameObject.SetActive(true);
                m_Playersanimatir.SetTrigger(MDance);
                transform.position= GameManager.Insatnce.m_Island.transform.position;
            }
        }
        else
        {
            if (other.gameObject.layer == 9)
            {
                JumpToNextPlatform();
            }else if (other.gameObject.layer == 10)
            {
                //dance
            }
            
        }
    }
    private void LateUpdate()
    {
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }
}
