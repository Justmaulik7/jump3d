﻿using UnityEngine;

public class CameraRotet : MonoBehaviour
{
    [SerializeField] private float Speed;

    void Update()
    {
       transform.Rotate(transform.up * Time.deltaTime * Speed );
    }
}
