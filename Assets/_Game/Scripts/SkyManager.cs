﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public class SkyManager : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_Sky;

    [SerializeField] private Transform m_Player;

    [ShowInInspector]
    private float m_Progress => Mathf.Clamp01(1-m_Player.position.y/-42);

    private Color m_TmpColor;

    private void Start()
    {
        m_TmpColor = Color.white;
    }

    private void Update()
    {
        m_TmpColor.a = m_Progress;
        m_Sky.color = m_TmpColor;
    }
}
