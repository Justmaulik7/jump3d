﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dreamteck.Splines;
using Sirenix.OdinInspector;
using UnityEngine;

public class Level : MonoBehaviour
{

    public SplineComputer Path;
    public GameObject Platforms;

#if  UNITY_EDITOR
    
    [SerializeField] private float Multi;
    
    [Button]
    private void Refs()
    {
        Path = transform.GetChild(0).GetComponent<SplineComputer>();
    }

    private void OnEnable()
    {
       GameManager.ActiveLevel = this;
    }

    [Button]
    private void DivideSpline()
    {
        List<SplinePoint> mPoints = new List<SplinePoint>();


        for (int i = 0; i < Path.CalculateLength(); i++)
        {
          var lPosition =   Path.Travel(0,i);
          mPoints.Add(new SplinePoint(){color = Color.white,size = 1,normal = Vector3.up,position = Path.Evaluate(lPosition).position});
        }
        Path.SetPoints(mPoints.ToArray());
    }

    [Button]
    private void SetDepth()
    {
        var lData = Path.GetPoints();
        for (int i = 0; i < lData.Length; i++)
        {
            lData[i].position.y = 0;
        }
        
        for (int i = 0; i < lData.Length; i++)
        {
            lData[i].position.y -= i*Multi;
        }
        
        Path.SetPoints(lData);
    }
#endif
}
