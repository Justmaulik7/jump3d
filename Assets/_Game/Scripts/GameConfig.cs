﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig")]
public class GameConfig : ScriptableObject
{
    public List<ColorsData> TrempolColorsData = new List<ColorsData>();
    [Header("Player")]
    public float MoveSpeed;
    public float RotateSpeed;
    public float JumpSpeed;
    public float PerfectJumpSpeed;
}

[System.Serializable]
public class ColorsData
{
    public Color ColorA;
    public Color ColorB;
}
