﻿
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HomeScreen : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_Visual;
    [SerializeField] private Button m_StartButton;


    [Button]
    private void Refs()
    {
        m_Visual = GetComponentInChildren<CanvasGroup>();
        m_StartButton = GetComponentInChildren<Button>();
    }

    public void Start()
    {
        
        m_Visual.gameObject.SetActive(true);
        m_Visual.DOFade(1, 0.25f);

        // if (PlayerPrefs.GetInt("AutoStart", 0) == 1)
        // {
        //     PlayerPrefs.SetInt("AutoStart",0);
        //     StartGame();
        // }
        m_StartButton.onClick.AddListener(StartGame);
    
    }


    private void StartGame()
    {
        GameManager.LevelStart?.Invoke();
        m_Visual.DOFade(0, 0.25f).OnComplete(() => { m_Visual.gameObject.SetActive(false);});
    }

    
}
