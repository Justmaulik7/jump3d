﻿
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour
{
    [SerializeField] private CanvasGroup m_Visual;
    [SerializeField] private Button m_ContinueButton;


    [Button]
    private void Refs()
    {
        m_Visual = GetComponentInChildren<CanvasGroup>();
        m_ContinueButton = GetComponentInChildren<Button>();
    }

    public void Start()
    {
        m_ContinueButton.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("AutoStart",1);
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            
        });
        GameManager.LevelWin.AddListener(() =>
        {
            m_Visual.gameObject.SetActive(true);
            m_Visual.alpha = 0;
            m_Visual.DOFade(1, 0.5f);

        });
        m_Visual.gameObject.SetActive(false);
    }

    
}
