﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	
	[SerializeField] private Transform camTransform;
	
	public static float shakeDuration = 0f;
	
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	private Vector3 m_originalPos;
	
	
	
	void OnEnable()
	{
		m_originalPos = camTransform.localPosition;
	}

	void Update()
	{
		if (shakeDuration > 0)
		{
			camTransform.localPosition = m_originalPos + Random.insideUnitSphere * shakeAmount;
			
			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;
			camTransform.localPosition = m_originalPos;
		}
	}
}