﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class effcts : MonoBehaviour
{
    [SerializeField] private float Size;
    [SerializeField] private float Time;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        Effctson();
    }

    void Effctson()
    {
        transform.DOScale(Vector3.one * Size, Time).OnComplete(() =>
        {
            transform.DOScale(Vector3.one, Time).OnComplete(Effctson);
        });
    }
}
